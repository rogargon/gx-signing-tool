import React, { useContext } from "react";
import {
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  Input,
  Text,
} from "@chakra-ui/react";
import { Container } from "./ui/Container";
import { AppContext } from "@/AppContext";
import { CodeEditor } from "./ui/CodeEditor";
import { PrivateKey } from "./PrivateKey";

export const Issuer: React.FC = () => {
  const {
    issuerDoc,
    setIssuerDoc,
    signDocument,
    signingIsLoading,
    verificationMethod,
    setVerificationMethod,
  } = useContext(AppContext);

  return (
    <Container minW={"40%"} maxW={"500px"} minH={"70vh"}>
      <Flex
        p={3}
        fontSize={20}
        w="100%"
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Heading size={"lg"} fontWeight={"bold"}>
          Issuer
        </Heading>
        <Button
          colorScheme="yellow"
          onClick={signDocument}
          disabled={!issuerDoc}
          isLoading={signingIsLoading}
          loadingText={"Issuing..."}
        >
          Issue
        </Button>
      </Flex>
      <Box padding={3}>
        <Flex alignItems={"center"} justifyContent={"space-between"} py={3}>
          <Text>Input document</Text>
        </Flex>
        <CodeEditor
          value={issuerDoc}
          onChange={(value) => setIssuerDoc(value || "")}
          height={"40vh"}
        />
        <Flex direction={"column"} mt={3}>
          <Text py={3}>Verification method</Text>
          <Input
            value={verificationMethod}
            onChange={(e) => setVerificationMethod(e.target.value)}
          />
        </Flex>
        <Divider mt={2} />
        <PrivateKey />
      </Box>
    </Container>
  );
};
