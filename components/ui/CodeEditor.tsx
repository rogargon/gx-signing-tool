import React from "react";
import Editor from "@monaco-editor/react";

interface CodeEditorProps {
  value: string;
  onChange?: (value: string) => void;
  height?: string;
}

export const CodeEditor: React.FC<CodeEditorProps> = ({
  value,
  onChange,
  height,
}) => {
  return (
    <Editor
      height={height || "50vh"}
      defaultLanguage="json"
      theme="light"
      value={value}
      options={{ lineNumbers: 'off', minimap: { enabled: false } }}
      onChange={(value) => (onChange ? onChange(value || "") : null)}
    />
  );
};
