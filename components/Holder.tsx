import React, { useContext, useEffect, useMemo, useState } from "react";
import { Button, Flex, Heading } from "@chakra-ui/react";
import { Container } from "./ui/Container";
import { AppContext } from "../AppContext";
import { SignedDocument } from "./SignedDocument";
import { CheckIcon, WarningIcon } from "@chakra-ui/icons";
import { useShapes } from "../hooks/useShapes";

export const Holder: React.FC = () => {
  const { signedDocuments } = useContext(AppContext);
  /*  const {} = useShapes();*/

  return (
    <Container minW={"40%"} maxW={"500px"} minH={"70vh"}>
      <Flex direction={"column"}>
        <Flex
          fontSize={20}
          w="100%"
          alignItems={"center"}
          justifyContent={"space-between"}
          p={3}
        >
          <Heading size={"lg"} fontWeight={"bold"}>
            Holder
          </Heading>
        </Flex>
        <Flex overflowY={"auto"} maxH={"85vh"} direction={"column"}>
          {Array.from(signedDocuments.entries()).map(
            ([key, signedDoc], index) => (
              <SignedDocument
                key={key}
                id={key}
                index={index}
                document={signedDoc}
              />
            )
          )}
        </Flex>
      </Flex>
    </Container>
  );
};
