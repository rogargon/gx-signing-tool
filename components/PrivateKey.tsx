import React from "react";
import { Container } from "./ui/Container";
import { Divider, Flex, Heading, Select } from "@chakra-ui/react";
import { CodeEditor } from "./ui/CodeEditor";
import { AppContext } from "../AppContext";

export const PrivateKey: React.FC = () => {
  const { privateKey, setPrivateKey } = React.useContext(AppContext);

  return (
    <Container width={"100"} mt={4} background={"none"} p={0} mx={0}>
      <Flex direction={"column"}>
        <Flex justifyContent={"space-between"} alignItems={"center"}>
          <Heading my={2} size={"lg"}>
            Private key
          </Heading>
        </Flex>
        <CodeEditor
          value={privateKey}
          onChange={setPrivateKey}
          height={"20vh"}
        />
        <Divider mt={2} />
      </Flex>
    </Container>
  );
};
