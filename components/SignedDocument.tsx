import React from "react";
import { Divider, Flex, Text } from "@chakra-ui/react";
import { CodeEditor } from "./ui/CodeEditor";

interface SignedDocumentProps {
  id: string;
  document: string;
  index: number;
}

export const SignedDocument: React.FC<SignedDocumentProps> = ({
  id,
  document,
  index,
}) => {
  return (
    <Flex key={id}>
      <Flex direction={"column"} p={3} w={"100%"}>
        <Flex justifyContent={"space-between"} alignItems={"center"} py={3}>
          <Text>Signed document {index + 1}</Text>
        </Flex>
        <Flex w={"100%"} direction={"column"}>
          <CodeEditor value={document} height={"40vh"} />
          <Divider mt={2} />
        </Flex>
      </Flex>
    </Flex>
  );
};
