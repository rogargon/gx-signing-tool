import React from "react";
import { Flex, Heading, Image } from "@chakra-ui/react";

export const Header: React.FC = () => {
  return (
    <Flex alignItems={"center"} px={3} py={5}>
      <Image
        src="https://www.data-infrastructure.eu/SiteGlobals/StyleBundles/Bilder/favicon-gaiax.ico;jsessionid=F7A72836C681272DD36450B159973944?__blob=normal&v=5"
        alt="logo"
        w={10}
        h={10}
        mx={3}
      />

      <Heading color={"white"}>Gaia-X Signing Tool</Heading>
    </Flex>
  );
};
