import React, { createContext, useState } from "react";
import { PemPrivateKeyExample } from "./data/keys/pemPrivateKeyExample";
import { useToast } from "@chakra-ui/react";
import { v4 as uuidv4 } from "uuid";
import { LegalParticipantExample } from "./data/doc";
import { signVerifiablePresentation } from "./web-crypto/signVerifiablePresentation";
import { signVerifiableCredential } from "./web-crypto/signVerifiableCredential";

export const AppContext = createContext<{
  issuerDoc: string;
  setIssuerDoc: (doc: string) => void;
  signedDocuments: Map<string, string>;
  setSignedDocuments: (docs: Map<string, string>) => void;
  privateKey: string;
  setPrivateKey: (key: string) => void;
  signDocument: () => void;
  signingIsLoading: boolean;
  verificationMethod: string;
  setVerificationMethod: (method: string) => void;
}>({} as any);

export const AppProvider = ({ children }: { children: React.ReactNode }) => {
  const toast = useToast();
  const [issuerDoc, setIssuerDoc] = useState<string>(
    JSON.stringify(LegalParticipantExample, null, 2)
  );
  const [key, setKey] = useState(PemPrivateKeyExample);
  const [verificationMethod, setVerificationMethod] = useState<string>(
      () => {
          try { return JSON.parse(issuerDoc).verifiableCredential[0].issuer; }
          catch (e: any) { return ""; }
      }
  );
  const [signedDocuments, setSignedDocuments] = useState<Map<string, string>>(
    new Map()
  );
  const [signingIsLoading, setSigningIsLoading] = useState<boolean>(false);

  const signDocument = async () => {
    try {
      setSigningIsLoading(true);

      let signedDocument;

      const doc = JSON.parse(issuerDoc);

      if (doc.type.includes("VerifiablePresentation")) {
        signedDocument = await signVerifiablePresentation(
          key,
          JSON.parse(issuerDoc),
          verificationMethod
        );
      } else if (doc.type.includes("VerifiableCredential")) {
        signedDocument = await signVerifiableCredential(
          key,
          JSON.parse(issuerDoc),
          verificationMethod
        );
      }

      setSignedDocuments(
        new Map(
          signedDocuments.set(uuidv4(), JSON.stringify(signedDocument, null, 2))
        )
      );

      setSigningIsLoading(false);
    } catch (err) {
      toast({
        title: "Error",
        description: "Error while signing document",
        status: "error",
        duration: 9000,
        isClosable: true,
      });
      console.log(err);
      setSigningIsLoading(false);
    }
  };

  return (
    <AppContext.Provider
      value={{
        issuerDoc,
        setIssuerDoc,
        signedDocuments,
        setSignedDocuments,
        privateKey: key,
        setPrivateKey: setKey,
        signDocument,
        signingIsLoading,
        verificationMethod,
        setVerificationMethod,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
