import { Header } from "@/components/Header";
import { Issuer } from "@/components/Issuer";
import { Holder } from "@/components/Holder";
import { Box, Flex } from "@chakra-ui/react";

export default function Home() {
  return (
    <div>
      <Box
        minH={"100vh"}
        bgGradient="linear(to-br, #43DAFC 0%, #642CF7 25%, #000041 100%)"
        px={8}
      >
        <Header />
        <Flex
          direction={["column", "column", "row"]}
          justifyContent={"space-around"}
        >
          <Issuer />
          <Holder />
        </Flex>
      </Box>
    </div>
  );
}
