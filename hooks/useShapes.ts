import axios from "axios";

interface FetchShapesResult {
  shapesLabels: string[];
}
const fetchShapes = async (): Promise<any> => {
  const url =
    "https://registry.lab.gaia-x.eu/v1-0-0/api/trusted-shape-registry/v1/shapes";
  const { data } = await axios.get<Record<string, any>>(url);

  return data;
};

export const useShapes = () => {
  // Get shapes labels
  /*const shapesLabels = Object.keys(data);

  return { shapesLabels };*/
};
