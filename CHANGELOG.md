## [1.0.4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.3...v1.0.4) (2023-04-12)


### Bug Fixes

* fix image name gen and deployment ([a968d2b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a968d2b2148fcceff09145b3f47ac6aab68d12a6))

## [1.0.3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.2...v1.0.3) (2023-04-12)


### Bug Fixes

* fix image name gen and deployment ([19bbe21](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/19bbe214a4e60cd0d5efeaa3fdac2d9d69378ce6))

## [1.0.2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.1...v1.0.2) (2023-04-12)


### Bug Fixes

* fix image name gen and deployment ([e5e5688](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e5e568881681f588a0ad322995df53c3925888f5))

## [1.0.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.0...v1.0.1) (2023-04-12)


### Bug Fixes

* fix docker image tag ([8341cda](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/8341cdaf04edd7e428497a01c276b618a5a86a3b))

# 1.0.0 (2023-04-12)


### Bug Fixes

* add generic did in VP example ([38dffe1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/38dffe102a14563f1c4d6e7a518ded47ddcca5b6))
* build ([212fd83](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/212fd8391f50e4d0e7c3a23c7c99615950b776ca))
* remove wrong import ([42599d6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/42599d66d6e09290643b001dd358e21fd9ce37b5))
* return type of signVerfiablePresentation ([272923f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/272923f5af4058a38241940580df2222c0e3e718))


### Features

* add gitignore ([4e9a724](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4e9a724dfa3e78551c129a2f2514ae89ec8cef68))
* add gx favicon ([cae8d64](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cae8d64511bee8eaaa73cdfe04a3dc8216f09bed))
* add README ([11b41ba](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/11b41ba364ee774d2e3c4acc97268cce5a7e960d))
* add title into html head ([f5d7a9f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f5d7a9fce1aa23a552adfe4a7481646f112973f0))
* add verification method field ([252d140](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/252d140e859a380b3a251d015dea04385b299a6d))
* client side VC signing ([426a838](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/426a83869205118c8fdcfda277b78a2d3edb55c4))
* create tool ([281c680](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/281c680fd4e08b17c2bbd5097a05020362d1186c))
* handle signing VP & VC ([9d9eea7](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9d9eea7c050d779d6a82df9793239ffbb5d52926))
* migrate on nextjs ([a199e8c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a199e8c8c87ceb1adb4dfcfe55efe749892b9744))
* responsive style ([5b759b3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5b759b30b98e48b35e0371b7d577149e5f614608))
