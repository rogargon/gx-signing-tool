# Gaia-X Signing tool

This is  a simple UI to sign Verifiable Credentials in a Verifiable Presentation on client side using an PEM private key without sending it through the network.

## Development

### Install

```bash
yarn
```

### Run

```bash
yarn dev
```
